
var Path = {};

Path.sourcePath = 'frontend/';
Path.destBasePath = 'public/';
Path.static = 'static/';
Path.destPath = Path.destBasePath + Path.static;

Path.src = {
	base: Path.sourcePath,
	jade:   [Path.sourcePath + 'jadeflux/**/*.jade',
			 '!' + Path.sourcePath + 'jadeflux/_**/*.jade',
			 '!' + Path.sourcePath + 'jadeflux/**/_*.jade'],
	coffee: [Path.sourcePath + 'coffee/**/*.coffee', 
			 Path.sourcePath + 'coffee/libs/**/*.coffee',
			 '!' + Path.sourcePath + 'coffee/_**/*.coffee',
			 '!' + Path.sourcePath + 'coffee/**/_*.coffee'],
	stylus: [Path.sourcePath + 'stylus/layouts/_render/*.styl', 
			 Path.sourcePath + 'stylus/**/*.styl',
			 '!' + Path.sourcePath + 'stylus/_**/*.styl',
			 '!' + Path.sourcePath + 'stylus/**/_*.styl'],
	lint:   [Path.destPath + 'js/source/**/*.js',
			 '!'+ Path.destPath +'/js/source/libs/**/*.js'],
	complexity: [Path.destPath + 'js/source/**/*.js',
				 '!'+ Path.destPath +'/js/source/libs/**/*.js'],
	copy: [Path.sourcePath + Path.static + 'fonts/**/*.*',
		   Path.sourcePath + Path.static + 'img/*.*',
		   Path.sourcePath + Path.static + 'img/**/*.*',
		   '!' + Path.sourcePath + Path.static + 'img/_**/*.*'],
	sprite: Path.sourcePath + Path.static + 'img/_sprite/*.png',
	concat : {
		javascript : [Path.destPath + 'js/source/scripts/**/*.js']
	}
};

Path.dest = {
	base: Path.destBasePath,
	jade: Path.destPath,
	coffee: Path.destPath + 'js/source/',
	javascript: Path.destPath + 'js/dist/scripts/',
	stylus: Path.destPath + 'css/',
	sprite: {
		stylus: Path.sourcePath + 'stylus/_mixins/',
		img: Path.destPath + 'img/',
	},
	clean: [Path.destPath + 'js/source/scripts/', 
			Path.destPath + 'js/dist/scripts/',
			Path.destPath + 'css/', 
			Path.destPath + 'modules/',
			Path.destPath + 'layout/',
			Path.destPath + 'fonts/',
			Path.destPath + 'img/',
			Path.destPath + 'index.html']
};

module.exports = Path;