
var locals = {};
var time = new Date().getTime();


//-static
locals.baseUrl = 'http://local.urbania3c.pe/';
//-locals.baseUrl = 'http://localhost:8080/';

locals.staticUrl = locals.baseUrl + 'static/';
locals.elementUrl = locals.baseUrl + 'elements/';
locals.version = time;

//-migration
//-locals.baseUrl = 'http://local.urbania.online/';
//-locals.staticUrl = locals.baseUrl + 'static/online/';
//-locals.elementUrl = locals.baseUrl + 'elements/online/';

module.exports = locals;

