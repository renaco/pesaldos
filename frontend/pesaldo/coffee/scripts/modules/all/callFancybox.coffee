###
Llamamos al Fancybox y definimos los modales
@class callFancybox
@main pagoefectivoSaldos/all
@author Paúl Díaz
###
yOSON.AppCore.addModule "callFancybox", (Sb) ->
	dom = {}
	st =
		btn_close	 : ".btn_close"
		fancyboxInline : ".fancybox_inline"
		steps_to_guide : "#btn_print_steps_to_guide"
		video : ".video"
	catchDom = ->
		dom.fancyboxInline = $(st.fancyboxInline)
		dom.steps_to_guide = $(st.steps_to_guide)
		dom.video = $(st.video)
		dom.btn_close = $(st.btn_close)
		return
	suscribeEvents = ->
		dom.btn_close.on 'click', fn.closeDialog
		return
	events =
		closeDialog: ->
			$.fancybox.close()
			return
	fn =
		callFancybox: ->
			if dom.fancyboxInline.length
				dom.fancyboxInline.fancybox(
					fitToView	: false
					autoSize	: true
					autoWidth 	: true
					width		: '65%'
					maxWidth	: '95%'
					closeClick	: false
					openEffect	: 'fade'
					closeEffect	: 'fade'
					padding		: [25, 20, 20, 20]
				)
			else
				if dom.steps_to_guide.length
					dom.steps_to_guide.fancybox(
						fitToView	: false
						autoSize	: true
						closeClick	: false
						openEffect	: 'fade'
						closeEffect	: 'fade'
						autoWidth 	: true
						padding		: [25, 27, 20, 27]
					)
				else
					dom.video.fancybox(
						fitToView	: false
						autoSize	: true
						closeClick	: false
						openEffect	: 'fade'
						closeEffect	: 'fade'
						padding		: [0, 0, 0, 0]
					)

			return

	initialize = (oP) ->
		$.extend st, oP
		catchDom()
		suscribeEvents()
		fn.callFancybox()
		return

	return {
		init: initialize
	}

, ['js/dist/libs/fancybox/source/jquery.fancybox.pack.js']
