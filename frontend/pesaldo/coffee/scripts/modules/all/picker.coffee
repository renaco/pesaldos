###
Crea el picker usando el plugin de jquery-ui
@class picker
@main pagoefectivoSaldos/All
@author Paúl Díaz, mfyance
###
yOSON.AppCore.addModule "picker", (Sb) ->

    st =
        picker : ".picker"
        pickerSpan : ".picker > span"
        txtExpiry : "#xCaducidadFake"
        txtExpiryHide : "#xDatoValidacion"

    dom = {}
    catchDom = ->
        dom.picker = $(st.picker)
        dom.pickerSpan = $(st.pickerSpan)
        dom.txtExpiry = $(st.txtExpiry)
        dom.txtExpiryHide = $(st.txtExpiryHide)
        return
    suscribeEvents = ->
        dom.pickerSpan.on 'click', events.clickPicker
        return
    events = 
        clickPicker: ->
            dom.txtExpiry.trigger 'focus'
            return
    functions =        
        picker: ->
            $.datepicker.setDefaults($.datepicker.regional["es"])
            dom.txtExpiry.datepicker(
                minDate : "+0d" 
                dateFormat: "dd/mm/y"
                altFormat: "dd/mm/yy"
                altField: dom.txtExpiryHide
                onSelect : ->
                    dom.txtExpiry.trigger 'blur'
                    return
                onClose: (selectedDate) ->
                    functions.aviso(selectedDate)
                    return
            )
            return
        aviso: (selectedDate) ->
            log "#{selectedDate} - El formator completo esta en el input '#xDatoValidacion'"
            return
    initialize = (oP) ->
        $.extend st, oP
        catchDom()
        suscribeEvents()
        functions.picker()
        return

    return {
        init: initialize
    }

, ['js/dist/libs/jquery-ui/jquery-ui.min.js', 'js/dist/libs/jquery-ui/ui/i18n/datepicker-es.js']