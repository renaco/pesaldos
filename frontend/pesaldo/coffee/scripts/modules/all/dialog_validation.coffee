###
Show the validation dialog
@class dialog_validation
@main pagoefectivoSaldos/all
@author mfyance
###
yOSON.AppCore.addModule "dialog_validation", (Sb) ->
    st =
        input               : "input.ajaxValidate"
        form                : ".form"
        boxalerta           : ".boxalerta"
        toloading           : ".toloading"
        loading             : ".toloading .loading"
        classShow           : "ns-show"
        classHide           : "ns-hide"
        toHide              : "tohide"
        toclose             : ".validandocontenido"
       
    dom = {}
    catchDom = ->
        dom.input           = $(st.input)
        dom.form            = $(st.form)
        dom.boxalerta       = $(st.boxalerta)
        dom.toloading       = $(st.toloading)
        dom.loading         = $(st.loading)
        dom.toclose         = $(st.toclose)
        return
    
    suscribeEvents = ->
        dom.input.on 'focusin', events.icoLoadingShow
        dom.input.on 'focusout', events.icoLoadingHide
        dom.toclose.on 'click', events.closeDialog
        return

    events =
        closeDialog: ->
            dom.boxalerta.removeClass(st.classShow).addClass(st.classHide)
            setTimeout(->
                dom.boxalerta.removeClass(st.classHide).addClass(st.toHide)
            , 300)
            return
        icoLoadingShow: ->
            $(@).prev().fadeIn()
            return
        icoLoadingHide: ->
            $(@).prev().fadeOut()
            return
        hideBox: ->
            dom.boxalerta.removeClass(st.classShow)
            dom.boxalerta.addClass(st.toHide)            
            return
        showBox: ->
            dom.boxalerta.removeClass(st.toHide)
            dom.boxalerta.addClass(st.classShow)            
            return
    initialize = (oP) ->
        $.extend st, oP
        catchDom()
        suscribeEvents()
        Sb.events(['showBox'], events.showBox, @)
        Sb.events(['hideBox'], events.hideBox, @)
        return

    return {
        init: initialize
    }