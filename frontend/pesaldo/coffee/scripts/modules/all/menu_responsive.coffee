###
Menu para diseño repsonsive
@class menu_responsive
@main pagoefectivoSaldos/all
@author mfyance
###
yOSON.AppCore.addModule "menu_responsive", (Sb) ->
    st =
        html        : "html"
        body        : "body"
        content     : ".content-wrap"
        openbtn     : "#open-button"
        closebtn    : "#close-button" 
    dom = {}
    catchDom = ->
        dom.html      =  $(st.html)
        dom.bodyEl      =  $(st.body)
        dom.content     = $(st.content)
        dom.openbtn     = $(st.openbtn)
        dom.closebtn    = $(st.closebtn)
        dom.isOpen = false
        return
    suscribeEvents = ->
        dom.openbtn.on 'click', events.toggleMenu 
        if dom.closebtn
            dom.closebtn.on 'click', events.toggleMenu        
        return
    events =
        toggleMenu : ->
            if dom.isOpen
                dom.bodyEl.removeClass('show-menu')
                dom.html.removeClass('oh')
            else
                dom.bodyEl.addClass('show-menu')
                dom.html.addClass('oh')
            dom.isOpen = !dom.isOpen
            return

    initialize = (oP) ->
        $.extend oP
        catchDom()
        suscribeEvents()
        return

    return {
        init: initialize
    }
, ['js/dist/libs/classie/classie.js']