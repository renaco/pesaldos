###
LocalStorage, permite tener un DB interno para hacer las pruebas
@class localStorage_data_base
@main pagoefectivoSaldos/all
@author mfyance
###
yOSON.AppCore.addModule "put_data", (Sb) ->
    st =
        form                : ".form"
        dni                 : "#xDocumento"
        fecha               : "#xCaducidadFake"
        nombre              : "#xNombres"
        paterno             : "#xApePaterno"
        materno             : "#xApeMaterno"
        email               : "#xEmail"
        mobile              : "#xMovil"
        clave1              : "#xPin"
        clave2              : "#xVerificarPin"
    dom = {}
    catchDom = ->
        dom.form        = $(st.form)
        dom.dni         = $(st.dni)
        dom.fecha       = $(st.fecha)
        dom.nombre      = $(st.nombre)
        dom.paterno     = $(st.paterno)
        dom.materno     = $(st.materno)
        dom.email       = $(st.email)
        dom.mobile      = $(st.mobile)
        dom.clave1      = $(st.clave1)
        dom.clave2      = $(st.clave2)
        return
    suscribeEvents = ->
        return
    fn =
        localStorage: ->
            yOSON.AppCore.addModule 'local_storage'
            yOSON.AppCore.runModule 'local_storage'

            setTimeout(->
                fn.putData()
            ,50)
            return
        putData: ->
            Sb.trigger('localStorageUsuario', (dataJSON) ->
                dom.dni.val(dataJSON.dni)
                dom.fecha.val(dataJSON.fecha)
                dom.nombre.val(dataJSON.nombre)
                dom.paterno.val(dataJSON.paterno)
                dom.materno.val(dataJSON.materno)
                dom.email.val(dataJSON.email)
                dom.mobile.val(dataJSON.mobile)
                dom.clave1.val(dataJSON.clave1)
                dom.clave2.val(dataJSON.clave2)
                log 'LS creado y datos copiados ;)'
                return
            )
            return

    initialize = (oP) ->
        $.extend st, oP
        catchDom()
        suscribeEvents()
        fn.localStorage()
        return
    return {
        init: initialize
    }