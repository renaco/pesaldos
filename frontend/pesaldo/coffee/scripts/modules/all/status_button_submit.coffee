###
Show the box alert
@class codigo_validate
@main pagoefectivoSaldos/all
@author mfyance
###
yOSON.AppCore.addModule "status_button_submit", (Sb) ->

    st =       
        form                : ".form"
        but_submit          : "button[type=submit]"
        cont_but            : ".button.button_arrow"
        but_class           : "btn"
        cont_but_class_def  : "button button_arrow"        
       
    dom = {}
    catchDom = ->
        dom.form            = $(st.form)
        dom.but_submit      = $(st.but_submit)
        dom.cont_but        = $(st.cont_but)
        return
    suscribeEvents = -> 
        return   
    fn =        
        resetStatus: ->
            dom.cont_but.removeAttr('class')
            return
        butStatus: (_nameClase) ->            
            # switch _nameClase
            #     when "active"    
            #         fn.resetStatus()                    
            #         dom.cont_but.addClass(st.cont_but_class_def+' '+_nameClase)
            #         #dom.but_submit.removeAttr('disabled')
            #         break
            #     when "unactive"
            #         fn.resetStatus()
            #         dom.cont_but.addClass(st.cont_but_class_def+' '+_nameClase)
            #         #dom.but_submit.attr('disabled', 'disabled')
            #         break
            return
    initialize = (oP) ->
        $.extend st, oP
        catchDom()
        suscribeEvents()
        Sb.events(['butStatus'], fn.butStatus, @)
        return

    return {
        init: initialize
    }