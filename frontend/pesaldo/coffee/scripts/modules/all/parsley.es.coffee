window.ParsleyConfig = window.ParsleyConfig or {}
window.ParsleyConfig.i18n = window.ParsleyConfig.i18n or {}
window.ParsleyConfig.i18n.es = $.extend(window.ParsleyConfig.i18n.es or {},
  defaultMessage: 'Este valor parece ser inválido.'
  type:
    email: 'Formato no válido'
    url: 'Formato no válido'
    number: 'Ingresar un número válido'
    integer: 'Ingresar un número válido'
    digits: 'Ingresar un dígito válido'
    alphanum: 'Ingresar un valor alfanumérico'
  notblank: 'Este valor no debe estar en blanco'
  required: 'Es requerido'
  pattern: 'Este valor es incorrecto'
  min: 'El valor no debe ser menor que %s'
  max: 'El valor no debe ser mayor que %s'
  range: 'El valor debe estar entre %s y %s'
  minlength: 'El valor es muy corto. Longitud mínima: %s caracteres'
  maxlength: 'El valor es muy largo. Longitud máxima: %s caracteres'
  length: 'La longitud de este valor debe estar entre %s y %s caracteres'
  mincheck: 'Debe seleccionar al menos %s opciones'
  maxcheck: 'Debe seleccionar %s opciones o menos'
  check: 'Debe seleccionar entre %s y %s opciones'
  equalto: 'No coinciden los valores')
if 'undefined' != typeof window.ParsleyValidator
  window.ParsleyValidator.addCatalog 'es', window.ParsleyConfig.i18n.es, true
