###
LocalStorage, permite tener un DB interno para hacer las pruebas
@class localStorage_data_base
@main pagoefectivoSaldos/all
@author mfyance
###
yOSON.AppCore.addModule "local_storage", (Sb) ->
    st =
        obj                 : 'mfyance'
        form                : '.form'
        dni                 : '12345678'
        fecha               : '14/10/10'
        nombre              : 'Moises'
        paterno             : 'Yance'
        materno             : 'Quispe'
        mobile              : '959273096'
        email               : 'moises.yance@ec.pe'
        clave1              : '123456'
        clave2              : '123456'
        edad                : '28'
    dom = {}
    catchDom = ->
        return
    suscribeEvents = ->
        return
    fn =
        localStorageUsuario:(data_base) ->
            usuario = {
                dni     : st.dni,
                fecha   : st.fecha,
                nombre  : st.nombre,
                paterno : st.paterno,
                materno : st.materno,
                mobile  : st.mobile,
                email   : st.email,
                clave1  : st.clave1,
                clave2  : st.clave2,
                edad    : st.edad
            }
            try
                if localStorage.getItem
                    localStorage.setItem(st.obj,JSON.stringify(usuario))
                    _user = localStorage.getItem(st.obj)
                    dataJSON = JSON.parse(_user)
                    data_base.call(this,dataJSON)

            catch e
                usuario = {}
            return

    initialize = (oP) ->
        $.extend st, oP
        catchDom()
        suscribeEvents()
        fn.localStorageUsuario()
        Sb.events(['localStorageUsuario'], fn.localStorageUsuario, @)
        return
    return {
        init: initialize
    }