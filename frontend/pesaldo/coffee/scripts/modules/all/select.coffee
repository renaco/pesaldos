###
Genera la funcionalidad para un select customizado
@class select
@main pagoefectivoSaldos/All
@author Paúl Díaz
###
yOSON.AppCore.addModule "select", (Sb) ->

    st =
        select : ".select"
        selectText : ".select .selectText"
        selectSelect : ".select select"

    dom = {}
    catchDom = ->
        dom.select = $(st.select)
        dom.selectText = $(st.selectText)
        dom.selectSelect = $(st.selectSelect)
        return
    suscribeEvents = ->
        dom.selectSelect.on 'change', fn.renderText
        return
    fn =
        getContextSelect : (sb)->
            _fn =
                getDataSelect:(context) ->
                    selected = $('option:selected', context)
                    id = context.selector
                    val = selected.val()
                    text = selected.text()
                    fn.pushData(id,val,text)
                    return
            sb.call(this,_fn)
            return
        pushData: (_id,_val, _text) ->
            $(_id).closest(dom.select).find(dom.selectText).text(_text)
            obj = {"id":_id,"val":_val,"text":_text}
            log obj
            return
        renderTitle : ->
            dom.selectSelect.each( (i, item) ->
                $this = $(item)
                title = $this.attr('title')
                if title.length
                    $this.siblings('.selectText').text(title)
                else
                    $this.find('option:selected').each( ->
                        $(@).prop('selected', true)
                        $this.siblings('.selectText').text($(@).text())
                        return
                    )
                return
            )
            return
        renderText : ->
            _this = $(@)
            val = _this.find('option:selected').text()
            _this.parents('.select').find('.selectText').text(val)
            setTimeout( ->
                log _this.val()
                if _this.hasClass('parsley-success') and _this.val() isnt "0"
                    _this.parents('.select').removeClass('parsley-error').addClass('parsley-success')
                return
            , 200)
            return
        disabledSelect : ->
            dom.selectSelect.each( (i, item) ->
                _this = $(item)
                log _this.val()
                if _this.hasClass('disabled')
                    _this.parent().removeClass('parsley-success').addClass('disabled')
                    val = _this.find('option:selected').text()
                    _this.parent('.select').find('.selectText').text(val)
                else
                    #log _this
                    _this.parent().removeClass('disabled').addClass('parsley-success')
                return
            )
        validateSelect : ->
            dom.selectSelect.each( (i, item) ->
                _this = $(item)
                #if _this.val() is "0"
                    #log _this.parent()
                    #_this.parent('.select').removeClass('parsley-success')
                return
            )
            return

    initialize = (oP) ->
        $.extend st, oP
        catchDom()
        suscribeEvents()
        fn.renderTitle()
        Sb.events(['disabledSelect'], fn.disabledSelect, this)
        Sb.events(['validateSelect'], fn.validateSelect, this)
        Sb.events(['pushData'], fn.pushData, this)
        Sb.events(['getContextSelect'], fn.getContextSelect, this)
        return

    return {
        init: initialize
    }