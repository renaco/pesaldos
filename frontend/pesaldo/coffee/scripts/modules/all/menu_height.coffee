###
Altura del menu
@class menu_height
@main pagoefectivoSaldos/all
@author Paúl Díaz
###
yOSON.AppCore.addModule "menu_height", (Sb) ->
	
	st =		
		block_menu : "#block_menu"

	dom = {}
	catchDom = ->
		dom.block_menu = $(st.block_menu)
		return
	suscribeEvents = ->
		$(window).on 'load resize', fn.equalHeight
		return
	fn = 		
		equalHeight: ->
			size = $(window).outerHeight()
			dom.block_menu.height(size)
			return

	initialize = (oP) ->
		$.extend st, oP
		catchDom()
		suscribeEvents()
		return

	return {
		init: initialize
	}