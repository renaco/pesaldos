###
Realiza la validación con parsleyjs.org
@class parsleyValidation
@main default/all
@author Ana Reyna, MFYance
###
yOSON.AppCore.addModule "parsleyValidation", (Sb) ->
	dom = {}
	st =
		form 				: "form"
		frm_class 			: ".frm"
		input	 			: ".frm input"
		fails 				: ".parsley-errors-list.filled"
		body 				: "body"
		button_submit		: "button[type=submit]"

	catchDom = ->
		dom.button = $(st.button)
		dom.fails = $(st.fails)
		dom.input = $(st.input)
		dom.form = $(st.form)
		dom.frm_class = $(st.frm_class)
		dom.body = $(st.body)
		dom.button_submit = $(st.button_submit)
		return
	suscribeEvents = ->
        dom.input.on 'focusout keypress keydown', fn.accountErrors
        dom.button_submit.on 'click', fn.accountErrors
        return
	fn =
		validate: ->
			window.ParsleyValidator.setLocale('es')
			return
		accountErrors: ->
			setTimeout(->
				#console.clear()
				obj_errors 			= {}
				total_errors		= 0
				$.each($(st.fails), (i, item) ->
					total_errors 	= ++i
					name 			= $(@).prev().attr('name')
					obj_errors[i] 	= name
					return
				)
				if total_errors isnt 0
					log "[x] Existen " + total_errors+ " errores"
					log obj_errors
					Sb.trigger("butStatus", "unactive")
				else
					Sb.trigger("butStatus", "active")
					log "[x] No hay errores"
			, 100)
			return
		callValidateSelect: ->
			Sb.trigger('validateSelect')
			return
	initialize = (oP) ->
		$.extend st, oP
		catchDom()
		suscribeEvents()
		fn.validate()
		fn.callValidateSelect()
		return

	return {
		init: initialize
	}

, ['js/dist/libs/parsleyjs/dist/parsley.min.js', 'js/source/scripts/modules/all/parsley.es.js']
