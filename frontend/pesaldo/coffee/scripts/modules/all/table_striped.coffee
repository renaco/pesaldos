###
Se da estilo striped a las tablas
@class tableStriped
@main pagoefectivoSaldos/all
@author Luis Natividad
###
yOSON.AppCore.addModule "tableStriped", (Sb) ->
	dom = {}
	st =
		table : ".table tbody tr:nth-child(2n+1)"
	catchDom = ->
		dom.table = $(st.table)
		return
	fn = 
		tableStriped: ->
			dom.table.addClass('odd')
			return

	initialize = (oP) ->
		$(document).ready( () ->
			$.extend st, oP
			catchDom()
			fn.tableStriped()
			return
		)

	return {
		init: initialize
	}


