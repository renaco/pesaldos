###
Dar interactividad a la info de usuario logueado
@class user_menu
@main pagoefectivoSaldos/peSaldos
@author mfyance
###
yOSON.AppCore.addModule "user_menu", (Sb) ->

    st =
        menu            : "#user_menu"
        content         : ".menu_content"
        html            : "html"
    dom = {}
    catchDom = ->
        dom.menu            = $(st.menu)
        dom.content         = $(st.content)
        dom.html            = $(st.html)
        return
    suscribeEvents = ->
        dom.menu.on 'click', events.myMenu
        dom.html.on 'click', events.outContext
        return
    events =
        outContext: (e) ->
            if (!$(e.target).closest(dom.content).length)
                log 'Hide T T'
                dom.menu.removeClass('active');
                dom.content.removeClass('fadeInUpSmall').delay(100).addClass('fadeOut');
            return
        myMenu: (e) ->
            e.stopPropagation()
            $(@).toggleClass('active')
            if($(@).hasClass('active'))
                dom.content.removeClass('fadeOut').delay(100).addClass('fadeInUpSmall')
            else
                dom.content.removeClass('fadeInUpSmall').delay(100).addClass('fadeOut')

            return

    initialize = (oP) ->
        $.extend st, oP
        catchDom()
        suscribeEvents()
        return

    return {
        init: initialize
    }