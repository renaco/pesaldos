###
Igualará las alturas de 2 elementos, siendo la altura final la del elemento más alto
@class equalingHeights
@main pagoefectivoSaldos/all
@author Paúl Díaz
###
yOSON.AppCore.addModule "equalingHeights", (Sb) ->
	
	st =		
		boxForm : ".box_form"
		boxIntroduction : ".box_introduction.equal"

	dom = {}
	catchDom = ->
		dom.boxForm = $(st.boxForm)
		dom.boxIntroduction = $(st.boxIntroduction)
		return
	suscribeEvents = ->
		$(window).on 'load resize', fn.controlWidth
		return

	fn = 
		controlWidth: ->
			w = $(window).width()
			if w > 959
				fn.callEqualingHeights()
			else
				dom.boxForm.removeAttr('style')
				dom.boxIntroduction.removeAttr('style')
			return		
		callEqualingHeights: ->
			fn.equalingHeights(dom.boxIntroduction, dom.boxForm)
			return
		equalingHeights : (firstElement, secondElement) ->
			firstElement.css(
				'min-height' : 0
			)
			secondElement.css(
				'min-height' : 0
			)
			fisrtH = firstElement.height()
			secondH = secondElement.height()
			if fisrtH > secondH
				secondElement.css(
					'min-height' : fisrtH
				)
			else
				firstElement.css(
					'min-height' : secondH
				)
			return

	initialize = (oP) ->
		$.extend st, oP
		catchDom()
		suscribeEvents()
		fn.equalingHeights(dom.boxIntroduction, dom.boxForm)
		Sb.events(['callEqualingHeights'], fn.callEqualingHeights, this)
		return

	return {
		init: initialize
	}