###
Functionalities to close block
@class close block
@main pagoefectivoSaldos/Login
@author Renaco
###
yOSON.AppCore.addModule "close_block", (Sb) ->
    st =
        box           :   ".alert_login"
        btn           :   ".alert_login_close"
       
    dom = {}
    catchDom = ->
        dom.box       = $(st.box)
        dom.btn       = $(st.btn)
        return

    suscribeEvents = ->
        dom.btn.on 'click', fn.close_box
        return 

    fn = 
        close_box: ->
            dom.box.hide()
            return

    initialize = (oP) ->
        $.extend st, oP
        catchDom()
        suscribeEvents()
        return

    return {
        init: initialize
    }
