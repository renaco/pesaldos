###
Functionallities to login
@class login
@main pagoefectivoSaldos/Account
@author mfyance
###
yOSON.AppCore.addModule "login", (Sb) ->
    st =
        mask            : ".mask"
        dni             : "#xDocumento"
        pass            : "#xPin"
        mobile          : "#xMovil"       
        submit          : "#btnLogIn"       
        token           : "[name=__RequestVerificationToken]"
        form            : ".frm"
        boxalerta       : ".boxalerta"
        msj_error       : "#msj_error input"
        body            : "body"
       
    dom = {}

    catchDom = ->
        dom.body            = $(st.body)
        dom.dni             = $(st.dni)
        dom.pass            = $(st.pass)
        dom.submit          = $(st.submit)
        dom.token           = $(st.token)
        dom.boxalerta       = $(st.boxalerta)
        dom.msj_error       = $(st.msj_error)
        return

    # suscribeEvents = ->
        # return 

    fn =
        validateData: -> 
            if dom.msj_error.val() is undefined
                log 'vacio'
                Sb.trigger("hideBox")
            else
                log 'lleno'
                Sb.trigger("showBox")
            return 
        
        maskPrice : ->
            dom.mask.number( true, 0 )
            log 'mask to numbeeeer'
            return

    initialize = (oP) ->
        $.extend st, oP
        catchDom()
        # suscribeEvents()
        fn.validateData()
        fn.maskPrice()
        return

    return {
        init: initialize 
    }

, ['js/dist/libs/jquery-number/jquery.number.min.js']
    