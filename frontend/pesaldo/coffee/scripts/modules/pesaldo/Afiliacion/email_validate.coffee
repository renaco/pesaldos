###
Validate the email
@class email_validate
@main pagoefectivoSaldos/Afiliacion
@author mfyance
###
yOSON.AppCore.addModule "email_validate", (Sb) ->
    st =
        email           : "#xEmail"
        token           : "[name=__RequestVerificationToken]"
        boxalerta       : ".boxalerta"
        msj_error       : "#msj_error"
        but_submit      : "button[type=submit]"
        length          : 1
    dom = {}
    catchDom = ->
        dom.email           = $(st.email)
        dom.token           = $(st.token)
        dom.boxalerta       = $(st.boxalerta)
        dom.msj_error       = $(st.msj_error)
        dom.but_submit      = $(st.but_submit)
        return
    suscribeEvents = ->
        dom.email.on 'keypress focusout', fn.validateCaracteres
        return
    fn =
        validateCaracteres: ->
            if dom.email.parsley().isValid() and dom.email.val().length >= st.length
                fn.validateUser()
            else
                Sb.trigger("hideBox")
            return
        validateUser: ->
            $.ajax
                url: 'Afiliacion/ValidarEmail'
                data:
                    xEmail: dom.email.val()
                    __RequestVerificationToken: dom.token.val()
                type: 'POST'
                dataType: 'JSON'
                cache: false
                beforeSend: ->
                success: (result) ->
                    if result.xError is 'ERROR'
                        dom.msj_error.text result.xMensaje
                        Sb.trigger 'showBox'
                        Sb.trigger 'butStatus', 'unactive'
                        window.ParsleyUI.addError(dom.email.parsley(), 'myajax', '.')
                        dom.but_submit.attr('disabled', 'disabled')
                    else
                        Sb.trigger 'hideBox'
                        Sb.trigger 'butStatus', 'active'
                        window.ParsleyUI.removeError(dom.email.parsley(), 'myajax')
                        dom.but_submit.removeAttr('disabled')
                    return
                error: (xhr, status, error) ->
                    log xhr.responseText
                    return
            return
    initialize = (oP) ->
        $.extend st, oP
        catchDom()
        suscribeEvents()
        return
    return {
        init: initialize
    }