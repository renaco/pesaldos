###
Refrescar el captcha
@class refresh_captcha
@main pagoefectivoSaldos/Afiliacion
@author mfyance
###
yOSON.AppCore.addModule "refresh_captcha", (Sb) ->
    st =
        but             : ".refresh"
        input           : "#xCaptcha"
        trigger         : "#imgRefreshCaptcha"
        img             : "#imgCaptcha"
    dom = {}
    catchDom = ->
        dom.but         = $(st.but)
        dom.input       = $(st.input)
        dom.trigger     = $(st.trigger)
        dom.img         = $(st.img)
        return
    suscribeEvents = ->
        dom.but.on 'click', functions.changeImage
        return
    functions =
        changeImage: ->
            dom.input.val('')
            dom.img.attr('src', 'Account/ShowCaptchaImage?' + Math.random());
            return
    initialize = (oP) ->
        $.extend st, oP
        catchDom()
        suscribeEvents()
        return
    return {
        init: initialize
    }