###
Ubigeo - Provincias/Departamentos
@class captcha_validate
@main pagoefectivoSaldos/Afiliacion
@author mfyance
###
yOSON.AppCore.addModule "ubigeo", (Sb) ->
    st =
        token           : "[name=__RequestVerificationToken]"
        form            : ".form"
        boxalerta       : ".boxalerta"
        msj_error       : "#msj_error"
        but_submit      : "button[type=submit]"
        depSel          : "#cDepartamentoSel"
        provSel         : "#cProvinciaSel"
        distSel         : "#cDistritoSel"

    dom = {}
    catchDom = ->
        dom.token           = $(st.token)
        dom.form            = $(st.form)
        dom.boxalerta       = $(st.boxalerta)
        dom.msj_error       = $(st.msj_error)
        dom.but_submit      = $(st.but_submit)
        dom.depSel          = $(st.depSel)
        dom.provSel         = $(st.provSel)
        dom.distSel         = $(st.distSel)
        return
    suscribeEvents = ->
        dom.depSel.on 'change', event.loadProvincias
        dom.provSel.on 'change', event.loadDistritos
        return
    event =
        loadPage:->
            event.viewOptionSelected(dom.depSel)
            event.loadProvincias()
            return
        resetValue: ->

            return
        viewOptionSelected: (_context) ->
            log _context
            Sb.trigger('getContextSelect', (_fn)->
                _fn.getDataSelect(_context)
            )
            return

        loadProvincias : ->
            $.ajax(
                url: '../../Afiliacion/lstProvincias'
                data:
                    cDepartamento: dom.depSel.val()
                    __RequestVerificationToken: dom.token.val()
                type: 'POST'
                dataType: 'JSON'
                cache: false
                beforeSend: ->
                    return
                success: (result) ->
                    template = $('#tmp_data_provincias').html()
                    info = Mustache.render(template, result)
                    dom.provSel.html(info)
                    return
                error: (xhr, status, error) ->
                    log xhr.responseText
            )
            .always ->
                event.viewOptionSelected(dom.provSel)
                return
            .done (response) ->
                event.loadDistritos()
                return
        loadDistritos :  ->
            $.ajax(
                url: '../../Afiliacion/lstDistritos'
                data:
                    cDepartamento: dom.depSel.val()
                    cProvincia: dom.provSel.val()
                    __RequestVerificationToken: dom.token.val()
                type: 'POST'
                dataType: 'JSON'
                cache: false
                beforeSend: ->
                    return
                success: (result) ->
                    template = $('#tmp_data_distritos').html()
                    info = Mustache.render(template, result)
                    dom.distSel.html(info)
                    return
                error: (xhr, status, error) ->
                    log xhr.responseText
                    return
            )
            .always ->
                event.viewOptionSelected(dom.distSel)
            .done ->
                return

    initialize = (oP) ->
        $.extend st, oP
        catchDom()
        suscribeEvents()
        event.loadPage()
        return
    return {
        init: initialize
    }
, ['js/dist/libs/mustache.js/mustache.js']