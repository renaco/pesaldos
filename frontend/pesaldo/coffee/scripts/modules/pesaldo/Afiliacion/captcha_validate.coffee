###
Show the captcha validation
@class captcha_validate
@main pagoefectivoSaldos/Afiliacion
@author mfyance
###
yOSON.AppCore.addModule "captcha_validate", (Sb) ->
    st =
        captcha         : "#xCaptcha"
        token           : "[name=__RequestVerificationToken]"
        form            : ".form"
        boxalerta       : ".boxalerta"
        msj_error       : "#msj_error"
        but_submit      : "button[type=submit]"
        length          : 6

    dom = {}
    catchDom = ->
        dom.captcha         = $(st.captcha)
        dom.token           = $(st.token)
        dom.form            = $(st.form)
        dom.boxalerta       = $(st.boxalerta)
        dom.msj_error       = $(st.msj_error)
        dom.but_submit      = $(st.but_submit)
        return
    suscribeEvents = ->
        dom.captcha.on 'focusout keypress', fn.validateCaracteres
        return
    fn =
        validateCaracteres: ->
            $('button[type=submit]').removeAttr('disabled')
            setTimeout(->
                if dom.captcha.parsley().isValid() and dom.captcha.val().length >= st.length
                    fn.validateUser()
                else
                    Sb.trigger("hideBox")
                return
            , 100)
            return
        validateUser: ->
            $.ajax
                url: 'Afiliacion/ValidarCodigoCaptcha'
                data:
                    xCaptcha: dom.captcha.val()
                    __RequestVerificationToken: dom.token.val()
                type: 'POST'
                dataType: 'JSON'
                cache: false
                beforeSend: ->
                    return
                success: (result) ->
                    if result.xError is 'ERROR'
                        dom.msj_error.text result.xMensaje
                        Sb.trigger 'showBox'
                        Sb.trigger 'butStatus', 'unactive'
                        window.ParsleyUI.addError(dom.captcha.parsley(), 'myajax', '.')
                        dom.but_submit.attr('disabled', 'disabled')
                    else
                        Sb.trigger 'hideBox'
                        Sb.trigger 'butStatus', 'active'
                        window.ParsleyUI.removeError(dom.captcha.parsley(), 'myajax')
                        dom.but_submit.removeAttr('disabled')
                    return
                error: (xhr, status, error) ->
                    log xhr.responseText
                    return
            return
    initialize = (oP) ->
        $.extend st, oP
        catchDom()
        suscribeEvents()
        return

    return {
        init: initialize
    }