###
Show the box alert
@class codigo_validate
@main pesaldos
@author MFYance
###
yOSON.AppCore.addModule "registrar_afiliacion", (Sb) ->

    st =
        code                 : "#xEmailConfirmacion"
        token               : "[name=__RequestVerificationToken]"
        form                : ".frm"
        boxalerta           : ".boxalerta"
        msj_error           : "#msj_error"
        toloading           : ".toloading"
        loading             : ".toloading .loading"
        submit              : "form [type=submit]"
        but_submit          : "button[type=submit]"
        length              :  5

    dom = {}
    catchDom = ->
        dom.code            = $(st.code)
        dom.form            = $(st.form)
        dom.boxalerta       = $(st.boxalerta)
        dom.toloading       = $(st.toloading)
        dom.loading         = $(st.loading)
        dom.submit          = $(st.submit)
        dom.but_submit      = $(st.but_submit)
        dom.msj_error       = $(st.msj_error)
        return
    suscribeEvents = ->
        dom.submit.on 'click', functions.validateCaracteres
        #dom.code.on 'keypress focusout', functions.validateCaracteres
        return
    functions =
        validateCaracteres: ->

            if dom.code.parsley().isValid() and dom.code.val().length >= st.length
                functions.validateUser()
            else
                Sb.trigger("hideBox")
            return
        validateUser: ->
            data = dom.form.serialize()
            $.ajax
                url: '../Afiliacion/RegistrarAfiliacion'
                data: data
                type: 'POST'
                dataType: 'JSON'
                cache: false
                beforeSend: ->
                    $('.capaloading').show()
                    return
                success: (result) ->
                    if result.xError is 'ERROR'
                        $('.capaloading').hide()
                        dom.msj_error.text result.xMensaje
                        Sb.trigger 'showBox'
                        Sb.trigger 'butStatus', 'unactive'
                        window.ParsleyUI.addError(dom.code.parsley(), 'myajax', '.')
                    else
                        Sb.trigger 'hideBox'
                        Sb.trigger 'butStatus', 'active'
                        window.ParsleyUI.removeError(dom.code.parsley(), 'myajax')
                        location.href = result.xRedirectUrl
                    return
                error: (xhr, status, error) ->
                    log xhr.responseText
                    return
            return
    initialize = (oP) ->
        $.extend st, oP
        catchDom()
        suscribeEvents()
        return

    return {
        init: initialize
    }