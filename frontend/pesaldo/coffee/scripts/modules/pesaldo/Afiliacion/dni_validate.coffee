###
Validate the DNI
@class dni_validate
@main pagoefectivoSaldos/Afiliacion
@author mfyance
###
yOSON.AppCore.addModule "dni_validate", (Sb) ->

    st =
        dni             : "#xDocumento"
        mobile          : "#xMovil"
        tipodoc         : "#iTipoDocumento"
        token           : "[name=__RequestVerificationToken]"
        boxalerta       : ".boxalerta"
        msj_error       : "#msj_error"
        but_submit      : "button[type=submit]"
        length          :  8

    dom = {}
    catchDom = ->
        dom.dni             = $(st.dni)
        dom.mobile          = $(st.mobile)
        dom.tipodoc         = $(st.tipodoc)
        dom.token           = $(st.token)
        dom.boxalerta       = $(st.boxalerta)
        dom.msj_error       = $(st.msj_error)
        dom.but_submit      = $(st.but_submit)
        return
    suscribeEvents = ->
        dom.dni.on 'keypress focusout', fn.validateCaracteres
        return
    fn =
        validateCaracteres: ->
            if dom.dni.parsley().isValid() and dom.dni.val().length >= st.length
                fn.validateUser()
            else
                Sb.trigger("hideBox")
            return
        validateUser: ->
            $.ajax
                url: 'Afiliacion/ValidarDocumento'
                data:
                    xDocumento: dom.dni.val()
                    iTipoDocumento: dom.tipodoc.val()
                    xMovil: dom.mobile.val()
                    __RequestVerificationToken: dom.token.val()
                type: 'POST'
                dataType: 'JSON'
                cache: false
                beforeSend: ->
                    return
                success: (result) ->
                    if result.xError is 'ERROR'
                        dom.msj_error.text result.xMensaje
                        Sb.trigger 'showBox'
                        Sb.trigger 'butStatus', 'unactive'
                        window.ParsleyUI.addError(dom.dni.parsley(), 'myajax', '.')
                        dom.but_submit.attr('disabled', 'disabled')
                    else
                        Sb.trigger 'hideBox'
                        Sb.trigger 'butStatus', 'active'
                        window.ParsleyUI.removeError(dom.dni.parsley(), 'myajax')
                        dom.but_submit.removeAttr('disabled')
                    return
                error: (xhr, status, error) ->
                    log xhr.responseText
                    return
            return
    initialize = (oP) ->
        $.extend st, oP
        catchDom()
        suscribeEvents()
        return

    return {
        init: initialize
    }