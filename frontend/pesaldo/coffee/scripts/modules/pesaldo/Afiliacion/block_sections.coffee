###
Active and Disabled the blocks
@class block_sections
@main pagoefectivoSaldos/Afiliacion
@author mfyance
###
yOSON.AppCore.addModule "block_sections", (Sb) ->
    st =
        frm                 : ".frm_step_2"
        block               : ".frm_step_2 .toblock"
        input               : ".frm_step_2 input:radio[name=lStand]"
        bloque_1            : "#recoger_tarjeta"
        bloque_2            : "#enviar_tarjeta"

        bloque_name_1       : "recoger_tarjeta"
        bloque_name_2       : "enviar_tarjeta"
        bloque_normal       : ".form_section.toblock"
        tag                 : "select ,  input[type=text]"

    dom = {}
    catchDom = ->
        dom.triggerClick    = $(st.triggerClick)
        dom.frm             = $(st.frm)
        dom.block           = $(st.block)
        dom.input           = $(st.input)
        dom.tag             = $(st.tag)

        dom.bloque_1        = $(st.bloque_1)
        dom.bloque_2        = $(st.bloque_2)
        dom.bloque_normal   = $(st.bloque_normal)
        return
    suscribeEvents = ->
        dom.input.on 'change', fn.changeInfo
        return
    fn =
        changeInfo: ->
            value       = $(@).val()
            bloque      = $(@).attr('data-view')
            fn.detectBlock(bloque)
            return
        detectBlock:(_bloque) ->
            switch _bloque
                when st.bloque_name_1
                    block_active = dom.bloque_1
                    fn.activeBlock(block_active)
                    break
                when st.bloque_name_2
                    block_active = dom.bloque_2
                    fn.activeBlock(block_active)
                    break
            return
        activeBlock: (_block_active) ->
            dom.bloque_normal.addClass('disabled')
            _block_active.removeClass('disabled')
            fn.changeStyles(_block_active)
            return
        changeStyles: (_block_active) ->

            $.each(dom.bloque_normal, ->
                $(@).find(dom.tag).attr('disabled', 'disabled')
                $(@).find(dom.tag).removeAttr('required','required')
                $(@).find(dom.tag).removeClass('parsley-error')
                $(@).find(dom.tag).next().children('li').hide()
            )

            $.each(_block_active, ->
                $(@).find(dom.tag).removeAttr('disabled', 'disabled')
                $(@).find(dom.tag).attr('required','required')
                $(@).find(dom.tag).next().children('li').show()
            )
            return

    initialize = (oP) ->
        $.extend st, oP
        catchDom()
        suscribeEvents()
        setTimeout(->
            fn.activeBlock(dom.bloque_1)
            return
        ,150)
        return

    return {
        init: initialize
    }