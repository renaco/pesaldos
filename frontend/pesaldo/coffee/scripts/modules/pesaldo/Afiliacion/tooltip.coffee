###
tooltip de un comunicado
@class tooltip
@main pagoefectivoSaldos/Afiliacion
@author mfyance
###
yOSON.AppCore.addModule "tooltip", (Sb) ->
    st =
        tooltip             : ".tooltip"
        tooltip_info        : ".tooltip > .info"
        tooltip_info_active : ".tooltip > .info.active"
    dom = {}
    catchDom = ->
        dom.tooltip             = $(st.tooltip)
        dom.tooltip_info        = $(st.tooltip_info)
        dom.tooltip_info_active = $(st.tooltip_info_active)
        return
    suscribeEvents = ->
        dom.tooltip.on 'click', fn.showTooltip
        dom.tooltip.on 'mouseout', fn.hideTooltip
        return
    fn =
        showTooltip: ->
            $(@).find(dom.tooltip_info).fadeIn()
            return
        hideTooltip: ->
            $(@).find(dom.tooltip_info).fadeOut()
            return
    initialize = (oP) ->
        $.extend st, oP
        catchDom()
        suscribeEvents()
        return

    return {
        init: initialize
    }