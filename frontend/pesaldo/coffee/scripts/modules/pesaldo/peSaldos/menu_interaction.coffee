###
Permite mostrar los tooltips al pasar el mouse por cada uno de los elementos del menu en la primera vez de acceso
@class menuInteraction
@main pagoefectivomovil/momo/peMovil
@author Luis Natividad
###
yOSON.AppCore.addModule "menuInteraction", (Sb) ->
	dom = {}
	st =
		item_list : "ul.menu_list li"
		tooltip_information : ".tooltip_information"
	catchDom = ->
		dom.item_list = $(st.item_list)
		dom.tooltip_information = $(st.tooltip_information)
		return
	suscribeEvents = ->
		dom.item_list.hover fn.open, fn.close
		return
	fn =
		open: ->
			_this = $(this)
			dom.tooltip_information.hide()
			_this.children(st.tooltip_information).show()
			return
		close: ->
			_this = $(this)
			dom.tooltip_information.hide()
			return
	initialize = (oP) ->
		$(document).ready( () ->
			$.extend st, oP
			catchDom()
			suscribeEvents()
			return
		)

	return {
		init: initialize
	}


