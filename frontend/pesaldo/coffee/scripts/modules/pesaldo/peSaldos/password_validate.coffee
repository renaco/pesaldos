###
Validate the password
@class password_validate
@main pagoefectivoSaldos/peSaldos
@author mfyance
###
yOSON.AppCore.addModule "password_validate", (Sb) ->

    st =
        clave           : "#xPin"
        boxalerta       : ".boxalerta"
        msj_error       : "#msj_error"
        but_submit      : "button[type=submit]"
        length          : 6

    dom = {}
    catchDom = ->
        dom.clave           = $(st.clave)
        dom.boxalerta       = $(st.boxalerta)
        dom.msj_error       = $(st.msj_error)
        dom.but_submit      = $(st.but_submit)
        return
    suscribeEvents = ->
        dom.clave.on 'keypress focusout', fn.validateCaracteres
        return
    fn =
        validateCaracteres: ->
            if dom.clave.parsley().isValid() and dom.clave.val().length >= st.length
                fn.validateUser()
            else
                Sb.trigger("hideBox")
            return
        validateUser: ->
            $.ajax
                url: '../../Common/ValidarPasswordInseguro'
                data:
                    xPin: dom.clave.val()
                type: 'POST'
                dataType: 'JSON'
                cache: false
                beforeSend: ->
                	return
                success: (result) ->
                    if result.xError is 'ERROR'
                        dom.msj_error.text result.xMensaje
                        Sb.trigger 'showBox'
                        Sb.trigger 'butStatus', 'unactive'
                        window.ParsleyUI.addError(dom.clave.parsley(), 'myajax', '.')
                        dom.but_submit.attr('disabled', 'disabled')
                    else
                        Sb.trigger 'hideBox'
                        Sb.trigger 'butStatus', 'active'
                        window.ParsleyUI.removeError(dom.clave.parsley(), 'myajax')
                        dom.but_submit.removeAttr('disabled')
                    return
                error: (xhr, status, error) ->
                    log xhr.responseText
                    return
            return
    initialize = (oP) ->
        $.extend st, oP
        catchDom()
        suscribeEvents()
        return

    return {
        init: initialize
    }