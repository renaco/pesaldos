###
Select personalizado
@class select
@main pesaldo/peMovil
@author Luis Natividad
###

yOSON.AppCore.addModule "select_data", (Sb) ->
	dom = {}
	st =
		option_selected : ".option_selected"
		content_options : ".content_options"
		option : ".option"
		option_selected_text : ".option_selected_text"
	catchDom = ->
		dom.option_selected = $(st.option_selected)
		dom.content_options = $(st.content_options)
		dom.option = $(st.option)
		dom.option_selected_text = $(st.option_selected_text)
		return
	suscribeEvents = ->
		dom.option_selected.on "click", events.watchOptions
		dom.option.on "click", events.selectOption
		return
	events =
		watchOptions: ->
			$(this).next(st.content_options).addClass "active"
			return
		selectOption: ->
			$(this).parent().parent().prev().children(st.option_selected_text).html $(this).html()
			dom.content_options.removeClass "active"
			return
	
	initialize = (oP) ->
		$(document).ready( () ->
			$.extend st, oP
			catchDom()
			suscribeEvents()
			return
		)

	return {
		init: initialize
	}


