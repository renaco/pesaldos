###
Select personalizado
@class select_customized
@main pesaldo/peSaldos
@author Luis Natividad
###
yOSON.AppCore.addModule "select_customized", (Sb) ->
	dom = {}
	st =
		select_title : ".select_title"
		select_content  : ".select_content"
		option : ".select_content ul li"
		icon_option : ".download_icon"
		text_option : ".text_option"
		text_select_title : ".text_select_title"
	catchDom = ->
		dom.select_title = $(st.select_title)
		dom.select_content = $(st.select_content)
		dom.option = $(st.option)
		dom.text_select_title = $(st.text_select_title)
		return
	suscribeEvents = ->
		dom.select_title.on "click", events.watchOptions
		dom.option.hover events.hoverInOption, events.hoverOutOption
		dom.option.on "click", events.selectOption
		return
	events =
		watchOptions: ->
			dom.select_content.show()
			return
		hoverInOption: ->
			log "hoverInOption"
			$(this).children(st.icon_option).show()
			return
		hoverOutOption: ->
			log "hoverOutOption"
			$(this).children(st.icon_option).hide()
			return
		selectOption: ->
			dom.text_select_title.text $(this).children(st.text_option).html()
			dom.select_content.hide()
			return

	initialize = (oP) ->
		$(document).ready( () ->
			$.extend st, oP
			catchDom()
			suscribeEvents()
			return
		)

	return {
		init: initialize
	}


