###
Permite crear un accordion
@class slick_slider
@main pagoefectivosaldos/dashboard
@author Renaco
###
yOSON.AppCore.addModule "slick_slider", (Sb) ->
  st = 
    dashboard : ".dashboard_slider"
  dom = {}

  catchDom = ->
    dom.dashboard = $(st.dashboard)
    return 

  afterCatchDom = ->
    $(st.dashboard).slick()
    log "test with module" 
    return

  initialize = (oP) ->
    $(document).ready( () -> 
      # $.extend st, oP
      catchDom()
      afterCatchDom()
      # suscribeEvents()
      return
    )

  return {
    init: initialize
  }
, ["js/dist/libs/slick-carousel/slick/slick.js"]