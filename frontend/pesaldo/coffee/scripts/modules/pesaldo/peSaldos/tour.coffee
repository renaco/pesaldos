###
Comportamiento del tour de enseñar como usar la app
@class tour
@main pesaldo/peSaldos
@author Luis Natividad
###
yOSON.AppCore.addModule "tour", (Sb) ->
	dom = {}
	st =
		btn_tour : ".tour"
		overlay  : ".overlay"
		steps_guide : ".steps_guide"
		step_one : ".step_one"
		step_two : ".step_two"
		step_three : ".step_three"
		step_four : ".step_four"
		step_next_one : ".step_one .next"
		step_prev_two : ".step_two .prev"
		step_next_two : ".step_two .next"
		step_prev_three : ".step_three .prev"
		step_next_three : ".step_three .next"
		step_prev_four : ".step_four .prev"
		step_next_four : ".step_four .next"
		step : ".step"
	catchDom = ->
		dom.btn_tour = $(st.btn_tour)
		dom.overlay = $(st.overlay)
		dom.steps_guide = $(st.steps_guide)
		dom.step_one = $(st.step_one)
		dom.step_two = $(st.step_two)
		dom.step_three = $(st.step_three)
		dom.step_four = $(st.step_four)
		dom.step_next_one = $(st.step_next_one)
		dom.step_prev_two = $(st.step_prev_two)
		dom.step_next_two = $(st.step_next_two)
		dom.step_prev_three = $(st.step_prev_three)
		dom.step_next_three = $(st.step_next_three)
		dom.step_prev_four = $(st.step_prev_four)
		dom.step_next_four = $(st.step_next_four)
		dom.step = $(st.step)

		return
	suscribeEvents = ->
		dom.btn_tour.on "click", events.startTour
		dom.step_next_one.on "click", events.openStepTwo
		dom.step_prev_two.on "click", events.openStepOne
		dom.step_next_two.on "click", events.openStepThree
		dom.step_prev_three.on "click", events.openStepTwo
		dom.step_next_three.on "click", events.openStepFour
		dom.step_prev_four.on "click", events.openStepThree
		dom.step_next_four.on "click", events.closeTour
		dom.overlay.on "click", events.closeOverlay
		return
	events =
		startTour: ->
			dom.overlay.addClass "active"
			dom.overlay.height $(document).height()
			dom.step_one.show()
			dom.steps_guide.show()
			$('html, body').animate({scrollTop: 0}, 500)
			return
		openStepOne: (e)->
			e.stopPropagation()
			dom.step_one.show()
			$(this).parent().parent().hide()
			return
		openStepTwo: (e)->
			e.stopPropagation()
			dom.step_two.show()
			$(this).parent().parent().hide()
			return
		openStepThree: (e)->
			e.stopPropagation()
			dom.step_three.show()
			$(this).parent().parent().hide()
			return
		openStepFour: (e)->
			e.stopPropagation()
			dom.step_four.show()
			$(this).parent().parent().hide()
			return
		closeTour: ->
			$(this).parent().parent().hide()
			dom.overlay.removeClass "active"
			return
		closeOverlay: ->
			dom.overlay.removeClass "active"
			dom.step.hide()
			return
	initialize = (oP) ->
		$(document).ready( () ->
			$.extend st, oP
			catchDom()
			suscribeEvents()
			return
		)

	return {
		init: initialize
	}


