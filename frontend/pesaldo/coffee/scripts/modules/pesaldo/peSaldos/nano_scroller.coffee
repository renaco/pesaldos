###
Cambiar la apariencia del scroll
@class nanoScroll
@main pagoefectivoSaldos/peSaldos
@author Luis Natividad
###
yOSON.AppCore.addModule "nanoScroll", (Sb) ->
	dom = {}
	st =
		nano : ".nano"
	catchDom = ->
		dom.nano = $(st.nano)
		return
	functions =
		scroll: ->
			dom.nano.nanoScroller({
				preventPageScrolling: true
				iOSNativeScrolling: true
				contentClass: 'nano_content'
			})
			return
	initialize = (oP) ->
		$(document).ready( () ->
			$.extend st, oP
			catchDom()
			functions.scroll()
			return
		)

	return {
		init: initialize
	}
, ["js/dist/libs/jquery.nanoscroller.js"]


