###
Validar captcha
@class actualizar_recarga
@main pagoefectivoSaldos/peSaldos
@author mfyance
###
yOSON.AppCore.addModule "captcha_validate", (Sb) ->
    st =
        captcha         : "#xCaptcha"
        token           : "[name=__RequestVerificationToken]"
        boxalerta       : ".boxalerta"
        msj_error       : ".msj_error"
        length          : 6

    dom = {}
    catchDom = ->
        dom.captcha         = $(st.captcha)
        dom.token           = $(st.token)
        dom.boxalerta       = $(st.boxalerta)
        dom.msj_error       = $(st.msj_error)
        return
    suscribeEvents = ->
        dom.captcha.on 'focusout keypress', fn.validateCaracteres
        return
    fn =
        validateCaracteres: ->
            $('button[type=submit]').removeAttr('disabled')
            setTimeout(->
                if dom.captcha.parsley().isValid() and dom.captcha.val().length >= st.length
                    fn.validateUser()
                else
                    Sb.trigger("hideBox")
                return
            , 100)
            return
        validateUser: ->
            $.ajax
                url: '../../Afiliacion/ValidarCodigoCaptcha'
                data:
                    xCaptcha: dom.captcha.val()
                    __RequestVerificationToken: dom.token.val()
                type: 'POST'
                dataType: 'JSON'
                cache: false
                beforeSend: ->
                    return
                success: (result) ->
                    if result.xError is 'ERROR'
                        Sb.trigger 'showBox'
                        Sb.trigger 'butStatus', 'unactive'
                        $('#msj_error').text result.xMensaje
                        dom.captcha.removeClass('parsley-success').addClass('parsley-error')
                        $('button[type=submit]').attr('disabled','disabled')
                    else
                        Sb.trigger 'hideBox'
                        Sb.trigger 'butStatus', 'active'
                    return
                error: (xhr, status, error) ->
                    log xhr.responseText
                    return
            return
    initialize = (oP) ->
        $.extend st, oP
        catchDom()
        suscribeEvents()
        return

    return {
        init: initialize
    }