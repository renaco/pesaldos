###
Actualzizar una recarga de un usuario
@class actualizar_recarga
@main pagoefectivoSaldos/peSaldos
@author mfyance
###
yOSON.AppCore.addModule "actualizar_recarga", (Sb) ->
    st =
        mask            : ".mask"
        anadir          : "AÑADIR"
        modificar       : "MODIFICAR"
        btn             : "#btn_add_rechange"
        contenedor      : ".recharge_mount"
        input           : ".inp_price"
        input_h         : ".inp_price_hidden"
        monto           : ".monto i"
        iMontoRecarga   : "#iMontoRecarga"
        iMoneda         : "#iMoneda"
        but_submit      : "button[type=submit]"
        token           : "input[name=__RequestVerificationToken]"
        boxalerta       : ".boxalerta"
        msj_error       : "#msj_error"
        length          : 1
    dom = {}
    catchDom = ->
        dom.mask            = $(st.mask)
        dom.btn             = $(st.btn)
        dom.contenedor      = $(st.contenedor)
        dom.input           = $(st.input)
        dom.input_h         = $(st.input_h)
        dom.monto           = $(st.monto)
        dom.token           = $(st.token)
        dom.iMontoRecarga   = $(st.iMontoRecarga)
        dom.iMoneda         = $(st.iMoneda)
        dom.but_submit      = $(st.but_submit)
        dom.boxalerta       = $(st.boxalerta)
        dom.msj_error       = $(st.msj_error)
        return
    suscribeEvents = ->
        dom.btn.on 'click', fn.addRechange
        return
    fn =
        addRechange: ->
            data = $(@).data('rel')
            $(@).toggleClass('active')
            switch data
                when st.anadir
                    $(@).closest(dom.contenedor).removeClass('noedit')
                    $(@).text(st.anadir)
                    $(@).data('rel',st.modificar)
                    dom.input.removeAttr('readonly')
                    break
                when st.modificar
                    $(@).closest(dom.contenedor).addClass('noedit')
                    $(@).text(st.modificar)
                    $(@).data('rel',st.anadir)
                    dom.input.attr('readonly','readonly')
                    $.ajax
                        url: '../peSaldos/ModificarMontosRecarga'
                        data:
                            iMontoRecarga: dom.iMontoRecarga.val()
                            iMoneda: dom.iMoneda.val()
                            mMonto: dom.input.val()
                            __RequestVerificationToken: dom.token.val()
                        type: 'POST'
                        dataType: 'JSON'
                        cache: false
                        beforeSend: ->
                            dom.monto.html "<img src='/Content/static/pesaldo/img/loading.gif' class='miniloading'></img>"
                            return
                        success: (result) ->
                            if result.xError is 'ERROR'
                                dom.msj_error.text result.xMensaje
                                Sb.trigger 'showBox'
                                Sb.trigger 'butStatus', 'unactive'
                                dom.input.prop('value',dom.input_h.val())
                                dom.monto.text dom.input_h.val()
                            else
                                log "Cambios correcto"
                                Sb.trigger 'hideBox'
                                Sb.trigger 'butStatus', 'active'
                                dom.input_h.prop('value',dom.input.val())
                                dom.monto.text dom.input.val()
                            return
                        error: (xhr, status, error) ->
                            log xhr.responseText
                            return
                    break
            return
        maskPrice : ->
            dom.mask.number( true, 0 )
            return
    initialize = (oP) ->
        $.extend st, oP
        catchDom()
        suscribeEvents()
        fn.maskPrice()
        return

    return {
        init: initialize
    }
, ['js/dist/libs/jquery-number/jquery.number.min.js']