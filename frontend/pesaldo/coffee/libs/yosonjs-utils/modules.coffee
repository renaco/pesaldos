yOSON.AppSchema.modules =
	"pesaldo":
		"controllers":
			"Home":
				"allActions": () ->
					return
				"actions":
					"Index": () ->
						log 'Home/Index'
						return
					"About": () ->
						log 'Home/About'
						return
					"Contact": () ->
						log 'Home/Contact'
						return
					"byDefault": () ->
						return
			"Account":
				"allActions": () ->
					return
				"actions":
					"Login": () ->
						yOSON.AppCore.runModule "close_block"
						yOSON.AppCore.runModule "forgotPin"
						yOSON.AppCore.runModule "login"
						return
					"byDefault": () ->
						return
			"Afiliacion":
				"allActions": () ->
					yOSON.AppCore.runModule "tooltip"
					return
				"actions":
					"AfiliacionPaso1": () ->
						yOSON.AppCore.runModule "dni_validate"
						yOSON.AppCore.runModule "mobile_validate"
						yOSON.AppCore.runModule "email_validate"
						yOSON.AppCore.runModule "password_validate"
						yOSON.AppCore.runModule "captcha_validate"
						yOSON.AppCore.runModule "refresh_captcha"
						return
					"AfiliacionPaso2": () ->
						yOSON.AppCore.runModule "block_sections"
						yOSON.AppCore.runModule "ubigeo"
						return
					"AfiliacionPaso3": () ->
						yOSON.AppCore.runModule "registrar_afiliacion"
						return
					"VerificarCuenta": () ->
						return
					"byDefault": () ->
						return
			"peSaldos":
				"allActions": () ->
					yOSON.AppCore.runModule "user_menu"
					return
				"actions":
					"Dashboard": () ->
						yOSON.AppCore.runModule "modificar_recarga"
						return
					"Recargas": () ->
						yOSON.AppCore.runModule "modificar_recarga"
						return
					"VerificarCuentaStand": () ->
						yOSON.AppCore.runModule "captcha_validate"
						yOSON.AppCore.runModule "refresh_captcha"
						yOSON.AppCore.runModule "password_validate"
						return
					"AfiliacionBienvenido": () ->
						yOSON.AppCore.runModule "menuInteraction"
						yOSON.AppCore.runModule "tour"
						return
					"SaldosyMovimientos": () ->
						yOSON.AppCore.runModule "nanoScroll"
						yOSON.AppCore.runModule "select_customized"
						return
					"EditarDatos": () ->
						yOSON.AppCore.runModule "select_data"
					"byDefault": () ->
						return
			"byDefault": () ->
				return
		"byDefault": () ->
			return
		"allControllers": () ->
			return
	"byDefault": () ->
		return
	"allModules": () ->
		yOSON.AppCore.runModule "select"
		yOSON.AppCore.runModule "makeNumericKeypad"
		# yOSON.AppCore.runModule "delegateWidthButtons" 
		yOSON.AppCore.runModule "equalingHeights"
		yOSON.AppCore.runModule "picker"
		yOSON.AppCore.runModule "callFancybox"
		yOSON.AppCore.runModule "parsleyValidation"
		yOSON.AppCore.runModule "tableStriped"
		yOSON.AppCore.runModule "accordion"
		yOSON.AppCore.runModule "addMsie"
		yOSON.AppCore.runModule "initAttrChange"
		yOSON.AppCore.runModule "reloadSelectivizr"
		yOSON.AppCore.runModule "dialog_validation"
		yOSON.AppCore.runModule "status_button_submit"
		yOSON.AppCore.runModule "menu_responsive"
		yOSON.AppCore.runModule "menu_height"
		return